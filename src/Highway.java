import java.util.Scanner;

import javax.swing.JTextArea;


/**
 * Description of Highway
 * 
 * @author b98505039
 * 
 */
public class Highway {

    private Integer len;
    private Car[] marks = new Car[1000];
    private Car recCar;
    private JTextArea out;

    /**
     * Highway Constructor
     * 
     * @param length
     *            length of the highway
     * 
     */
    public Highway(Integer length, JTextArea outputArea) {
	len = length;
	recCar = null;
	out = outputArea;
    }

    
    /** Step 1: Print all cars. */
    public void printCars() {
	int i;
	for (i = 0; i < len; i++) {
	    if (marks[i] == null)
		out.append(".");
	    else {
		out.append("x");
	    }
	}
	out.append("\n");
	return;
    }

    /**
     * Step 2: Put a new car from the beginning of highway.
     * 
     * @return true if car successfully put, otherwise false
     * 
     * */
    public boolean putNew() {
	// first car
	if (recCar == null) {
	    marks[0] = new Car();
	    marks[0].prevCar = null;

	    marks[0].speed = 4;
	    marks[0].nextSpeed = 0;
	    marks[0].next2Speed = 0;
	    marks[0].pos = 1;
	    return true;
	} else if (recCar.pos == 2) {
	    // no new car this run
	    return false;
	} else {
	    marks[0] = new Car();
	    marks[0].prevCar = recCar;

	    Integer newSpeed = (int) Math.floor((recCar.pos - 1) / 2);
	    if (newSpeed > 4)
		marks[0].speed = 4;
	    marks[0].speed = newSpeed;
	    marks[0].nextSpeed = 0;
	    marks[0].next2Speed = 0;
	    marks[0].pos = 1;
	    return true;
	}
    }

    /**
     * Step 3: Put a new car from the interchange. (opt. for task 3)
     * 
     * @return true if car successfully put, otherwise false
     * 
     * */
    public boolean putItr() {
	int i, j = 0;
	// car closest & before 50
	for (i = 0; i < 50; i++) {
	    if (marks[i] != null)
		j = i;
	}

	Car prevItrCar = marks[j].prevCar;
	// first put, no previous car
	if (prevItrCar == null) {
	    marks[50] = new Car();
	    marks[50].prevCar = null;
	    marks[j].prevCar = marks[50];

	    marks[50].speed = 4;
	    marks[50].nextSpeed = 0;
	    marks[50].next2Speed = 0;
	    marks[50].pos = 50;
	    return true;
	} else {
	    if (prevItrCar.pos == 50 || prevItrCar.pos == 51)
		return false;

	    // put car at 50
	    marks[50] = new Car();
	    marks[50].prevCar = marks[j].prevCar;
	    marks[j].prevCar = marks[50];

	    Integer newSpeed = (int) Math
		    .floor((marks[50].prevCar.pos - 50) / 2);
	    if (newSpeed > 4)
		marks[0].speed = 4;
	    marks[50].speed = newSpeed;
	    marks[50].nextSpeed = 0;
	    marks[50].next2Speed = 0;
	    marks[50].pos = 50;
	    return true;
	}
    }

    /**
     * Step 4: Modifying the speed of every car.
     * 
     * @param mode
     *            normal modification or modify for task 2
     * 
     */
    public void modCars(Integer mode) {
	int i;
	for (i = 1; i < len; i++) {
	    if (marks[i] != null) {
		// for task 2
		if (marks[i].prevCar == null && mode == 2) {
		    marks[i].speed = 2;
		    marks[i].nextSpeed = 4;
		    return;
		}
		if (marks[i].next2Speed > 0) {
		    marks[i].nextSpeed = marks[i].next2Speed;
		    marks[i].next2Speed = 0;
		    continue;
		} else if (marks[i].nextSpeed > 0) {
		    marks[i].speed = marks[i].nextSpeed;
		    marks[i].nextSpeed = 0;
		    continue;
		}

		if (marks[i].prevCar != null) {
		    Integer newSpeed = (int) Math
			    .floor((marks[i].prevCar.pos - marks[i].pos) / 2);
		    if (newSpeed > 4)
			newSpeed = 4;
		    // speed up or down
		    if (newSpeed > marks[i].speed) {
			marks[i].next2Speed = newSpeed;
		    } else if (newSpeed < marks[i].speed) {
			marks[i].nextSpeed = newSpeed;
		    }
		}
	    }
	}
	return;
    }

    /** Step 6: Move cars by specified speed. */
    public boolean moveCars() {
	
	int i;
	for (i = len; i >= 0; i--) {
		if (marks[i] != null) {
			marks[i].pos += marks[i].speed;
			if (marks[i].prevCar != null && marks[i].pos >= marks[i].prevCar.pos) {
				return false;
			}
			if ((i + marks[i].speed) < len) {
			    marks[i + marks[i].speed] = marks[i];
			    recCar = marks[i + marks[i].speed];
			}
			marks[i] = null;
		}
	}
	return true;
	
	/*
	int i;
	for (i = len; i >= 0; i--) {
	    if (marks[i] != null) {
		final int idx = i;
		final Car c = marks[idx];
		Thread t = new Thread () {
		    public void run () {
			c.pos += c.speed;
		    }
		};
		t.start();
		try {
		    t.join();
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
		t = null;
		marks[idx].pos = c.pos;
		if (marks[idx].prevCar != null
			&& marks[idx].pos >= marks[idx].prevCar.pos) {
		    System.exit(0);
		}
		if ((idx + marks[idx].speed) < len) {
		    marks[idx + marks[idx].speed] = marks[idx];
		    recCar = marks[idx + marks[idx].speed];
		}
		marks[idx] = null;
	    }
	}
	//Thread.sleep(10);
	return;
	*/
    }
}
