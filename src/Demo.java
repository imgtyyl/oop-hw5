import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class Demo extends JApplet {
    
    private final JTextField[] tArr = new JTextField[3];
    private JRadioButton[] rArr = new JRadioButton[3];
    private JTextArea res = new JTextArea();
    //private final MyFrame mf;
    private int mode;
    private int len;
    private int nBig;
    private int nItr;

    public void init() {
        //Execute a job on the event-dispatching thread; creating this applet's GUI.
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                public void run() {
                    setSize(800, 600);
                    setLayout(new GridBagLayout());
                    //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   
                    //mf = this;
                    
                    JRadioButton r1 = new JRadioButton("Task 1");
                    GridBagConstraints c0 = new GridBagConstraints();
                    c0.gridx = 0;
                    c0.gridy = 0;
                    c0.gridwidth = 2;
                    c0.gridheight = 1;
                    c0.fill = GridBagConstraints.NONE;
                    c0.anchor = GridBagConstraints.CENTER;
                    
                    JRadioButton r2 = new JRadioButton("Task 2");
                    GridBagConstraints c1 = new GridBagConstraints();
                    c1.gridx = 2;
                    c1.gridy = 0;
                    c1.gridwidth = 2;
                    c1.gridheight = 1;
                    c1.fill = GridBagConstraints.NONE;
                    c1.anchor = GridBagConstraints.CENTER;
                    
                    JRadioButton r3 = new JRadioButton("Task 3");
                    GridBagConstraints c2 = new GridBagConstraints();
                    c2.gridx = 4;
                    c2.gridy = 0;
                    c2.gridwidth = 2;
                    c2.gridheight = 1;
                    c2.fill = GridBagConstraints.NONE;
                    c2.anchor = GridBagConstraints.CENTER;
                    
                    ButtonGroup group = new ButtonGroup();
                    rArr[0] = r1;
                    rArr[1] = r2;
                    rArr[2] = r3;
                    group.add(r1);
                    group.add(r2);
                    group.add(r3);
                    add(r1,c0);
                    add(r2,c1);
                    add(r3,c2);
                    
                    JLabel l1 = new JLabel("highway_len");
                    GridBagConstraints c4 = new GridBagConstraints();
                    c4.gridx = 0;
                    c4.gridy = 1;
                    c4.gridwidth = 3;
                    c4.gridheight = 1;
                    c4.fill = GridBagConstraints.NONE;
                    c4.anchor = GridBagConstraints.EAST;
                    add(l1,c4);
                    
                    JLabel l2 = new JLabel("num_car_at_beginning");
                    GridBagConstraints c5 = new GridBagConstraints();
                    c5.gridx = 0;
                    c5.gridy = 2;
                    c5.gridwidth = 3;
                    c5.gridheight = 1;
                    c5.fill = GridBagConstraints.NONE;
                    c5.anchor = GridBagConstraints.EAST;
                    add(l2,c5);
                    
                    JLabel l3 = new JLabel("num_car_at_intersection");
                    GridBagConstraints c6 = new GridBagConstraints();
                    c6.gridx = 0;
                    c6.gridy = 3;
                    c6.gridwidth = 3;
                    c6.gridheight = 1;
                    c6.fill = GridBagConstraints.NONE;
                    c6.anchor = GridBagConstraints.EAST;
                    add(l3,c6);
                    
                    int i;
                    for (i = 0; i < 3; i++) {
                        final int idx = i;
                        Thread t = new Thread () {
                            public void run () {
                                    JTextField t3 = new JTextField();
                                    GridBagConstraints c9 = new GridBagConstraints();
                                    c9.gridx = 3;
                                    c9.gridy = idx+1;
                                    c9.gridwidth = 3;
                                    c9.gridheight = 1;
                                    c9.fill = GridBagConstraints.HORIZONTAL;
                                    c9.anchor = GridBagConstraints.WEST;
                                    tArr[idx] = t3;
                                    add(t3,c9);
                            }
                        };
                        t.start();
                        try {
                            t.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        t = null;
                    }

                    JButton btn = new JButton("Go!");
                    GridBagConstraints c10 = new GridBagConstraints();
                    c10.gridx = 2;
                    c10.gridy = 4;
                    c10.gridwidth = 2;
                    c10.gridheight = 1;
                    c10.fill = GridBagConstraints.NONE;
                    c10.anchor = GridBagConstraints.CENTER;
                    add(btn,c10);
                    btn.addActionListener(new ActionListener() {
                	public void actionPerformed(ActionEvent event) {
                	        if (rArr[0].isSelected()) mode = 1;
                	        else if (rArr[1].isSelected()) mode = 2;
                	        else if (rArr[2].isSelected()) mode = 3;
                	        len = Integer.parseInt(tArr[0].getText());
                	        nBig = Integer.parseInt(tArr[1].getText());
                	        if(mode == 3) nItr = Integer.parseInt(tArr[2].getText());
                	        
                	        //System.out.println(len);
                	        
                	        res.setText("");
                	        //JFrame frm = new ResultFrame(500, 800, this);
                	        int run = 200;
                	        
                	        Highway hw = new Highway(len, res);
                	        int i;
                	        boolean putNewOk = false, putItr = false;
                	        
                	        for (i = 0; i < run; i++) {
                	                //s1
                	                //System.out.print(i);
                	                hw.printCars();
                	                //s2
                	                if (nBig > 0) 
                	                        putNewOk = hw.putNew();
                	                //s3
                	                if (mode == 3 && nItr > 0) 
                	                        putItr = hw.putItr();
                	                //s4
                	                if (i == 4 && mode == 2) 
                	                        hw.modCars(2);
                	                else 
                	                        hw.modCars(1);
                	                //s6
                	                if(!hw.moveCars()) break;
                	                
                	                if (putNewOk) 
                	                    nBig--;
                	                if (putItr) 
                	                    nItr--;     
                	        }
                	    }
                    });
                    
                    
                    JTextArea result = new JTextArea("");
                    GridBagConstraints c11 = new GridBagConstraints();
                    c11.gridx = 10;
                    c11.gridy = 0;
                    c11.gridwidth = 10;
                    c11.gridheight = 10;
                    c11.fill = GridBagConstraints.BOTH;
                    c11.anchor = GridBagConstraints.CENTER;
                    res = result;
                    add(result,c11);
                    
                    setVisible(true);
                }
            });
        } catch (Exception e) {
            System.err.println("createGUI didn't complete successfully");
        }
    }
    
    
}

class MyFrame extends JFrame implements ActionListener {
    
    //task_number highway_len num_car_at_beginning [num_car_at_intersection]
    private final JTextField[] tArr = new JTextField[3];
    private JRadioButton[] rArr = new JRadioButton[3];
    public JTextArea res = new JTextArea();
    public final MyFrame mf;
    public int mode;
    public int len;
    public int nBig;
    public int nItr;
    
    public MyFrame(int width, int height){
        setSize(width, height);
        this.setLayout(new GridBagLayout());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   
        mf = this;
        
        JRadioButton r1 = new JRadioButton("Task 1");
        GridBagConstraints c0 = new GridBagConstraints();
        c0.gridx = 0;
        c0.gridy = 0;
        c0.gridwidth = 2;
        c0.gridheight = 1;
        c0.fill = GridBagConstraints.NONE;
        c0.anchor = GridBagConstraints.CENTER;
        
        JRadioButton r2 = new JRadioButton("Task 2");
        GridBagConstraints c1 = new GridBagConstraints();
        c1.gridx = 2;
        c1.gridy = 0;
        c1.gridwidth = 2;
        c1.gridheight = 1;
        c1.fill = GridBagConstraints.NONE;
        c1.anchor = GridBagConstraints.CENTER;
        
        JRadioButton r3 = new JRadioButton("Task 3");
        GridBagConstraints c2 = new GridBagConstraints();
        c2.gridx = 4;
        c2.gridy = 0;
        c2.gridwidth = 2;
        c2.gridheight = 1;
        c2.fill = GridBagConstraints.NONE;
        c2.anchor = GridBagConstraints.CENTER;
        
        ButtonGroup group = new ButtonGroup();
        rArr[0] = r1;
        rArr[1] = r2;
        rArr[2] = r3;
        group.add(r1);
        group.add(r2);
        group.add(r3);
        this.add(r1,c0);
        this.add(r2,c1);
        this.add(r3,c2);
        
        JLabel l1 = new JLabel("highway_len");
        GridBagConstraints c4 = new GridBagConstraints();
        c4.gridx = 0;
        c4.gridy = 1;
        c4.gridwidth = 3;
        c4.gridheight = 1;
        c4.fill = GridBagConstraints.NONE;
        c4.anchor = GridBagConstraints.EAST;
        this.add(l1,c4);
        
        JLabel l2 = new JLabel("num_car_at_beginning");
        GridBagConstraints c5 = new GridBagConstraints();
        c5.gridx = 0;
        c5.gridy = 2;
        c5.gridwidth = 3;
        c5.gridheight = 1;
        c5.fill = GridBagConstraints.NONE;
        c5.anchor = GridBagConstraints.EAST;
        this.add(l2,c5);
        
        JLabel l3 = new JLabel("num_car_at_intersection");
        GridBagConstraints c6 = new GridBagConstraints();
        c6.gridx = 0;
        c6.gridy = 3;
        c6.gridwidth = 3;
        c6.gridheight = 1;
        c6.fill = GridBagConstraints.NONE;
        c6.anchor = GridBagConstraints.EAST;
        this.add(l3,c6);
        
        /*
        JTextField t1 = new JTextField();
        GridBagConstraints c7 = new GridBagConstraints();
        c7.gridx = 3;
        c7.gridy = 1;
        c7.gridwidth = 3;
        c7.gridheight = 1;
        c7.fill = GridBagConstraints.HORIZONTAL;
        c7.anchor = GridBagConstraints.WEST;
        tArr[0] = t1;
        this.add(t1,c7);
        
        JTextField t2 = new JTextField();
        GridBagConstraints c8 = new GridBagConstraints();
        c8.gridx = 3;
        c8.gridy = 2;
        c8.gridwidth = 3;
        c8.gridheight = 1;
        c8.fill = GridBagConstraints.HORIZONTAL;
        c8.anchor = GridBagConstraints.WEST;
        tArr[1] = t2;
        this.add(t2,c8);
        
        JTextField t3 = new JTextField();
        GridBagConstraints c9 = new GridBagConstraints();
        c9.gridx = 3;
        c9.gridy = 3;
        c9.gridwidth = 3;
        c9.gridheight = 1;
        c9.fill = GridBagConstraints.HORIZONTAL;
        c9.anchor = GridBagConstraints.WEST;
        tArr[2] = t3;
        this.add(t3,c9);
        */
        
        int i;
        for (i = 0; i < 3; i++) {
            final int idx = i;
            Thread t = new Thread () {
                public void run () {
                        JTextField t3 = new JTextField();
                        GridBagConstraints c9 = new GridBagConstraints();
                        c9.gridx = 3;
                        c9.gridy = idx+1;
                        c9.gridwidth = 3;
                        c9.gridheight = 1;
                        c9.fill = GridBagConstraints.HORIZONTAL;
                        c9.anchor = GridBagConstraints.WEST;
                        tArr[idx] = t3;
                        mf.add(t3,c9);
                }
            };
            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            t = null;
        }

        JButton btn = new JButton("Go!");
        GridBagConstraints c10 = new GridBagConstraints();
        c10.gridx = 2;
        c10.gridy = 4;
        c10.gridwidth = 2;
        c10.gridheight = 1;
        c10.fill = GridBagConstraints.NONE;
        c10.anchor = GridBagConstraints.CENTER;
        this.add(btn,c10);
        btn.addActionListener(this);
        
        
        JTextArea result = new JTextArea("");
        GridBagConstraints c11 = new GridBagConstraints();
        c11.gridx = 10;
        c11.gridy = 0;
        c11.gridwidth = 10;
        c11.gridheight = 10;
        c11.fill = GridBagConstraints.BOTH;
        c11.anchor = GridBagConstraints.CENTER;
        res = result;
        this.add(result,c11);
        
        setVisible(true);
    }
    
    public void actionPerformed(ActionEvent event) {
        if (rArr[0].isSelected()) mode = 1;
        else if (rArr[1].isSelected()) mode = 2;
        else if (rArr[2].isSelected()) mode = 3;
        len = Integer.parseInt(tArr[0].getText());
        nBig = Integer.parseInt(tArr[1].getText());
        if(mode == 3) nItr = Integer.parseInt(tArr[2].getText());
        
        //System.out.println(len);
        
        res.setText("");
        //JFrame frm = new ResultFrame(500, 800, this);
        int run = 200;
        
        Highway hw = new Highway(len, res);
        int i;
        boolean putNewOk = false, putItr = false;
        
        for (i = 0; i < run; i++) {
                //s1
                //System.out.print(i);
                hw.printCars();
                //s2
                if (nBig > 0) 
                        putNewOk = hw.putNew();
                //s3
                if (mode == 3 && nItr > 0) 
                        putItr = hw.putItr();
                //s4
                if (i == 4 && mode == 2) 
                        hw.modCars(2);
                else 
                        hw.modCars(1);
                //s6
                if(!hw.moveCars()) break;
                
                if (putNewOk) 
                    nBig--;
                if (putItr) 
                    nItr--;     
        }
    }
}