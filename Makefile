all:
	mkdir ./bin
	javac -cp ./class -sourcepath ./src -d ./bin ./src/*.java
	jar cvf ./Demo.jar -C bin /
clean:
	rm -rf ./bin/
	rm -rf ./Demo.jar
